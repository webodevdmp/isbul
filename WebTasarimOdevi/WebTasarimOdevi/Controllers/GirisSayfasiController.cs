﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebTasarimOdevi.Models;

namespace WebTasarimOdevi.Controllers
{
    public class GirisSayfasiController : Controller
    {
        // GET: GirisSayfasi
        public ActionResult GirisS()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Kullanicilar(WebTasarimOdevi.Models.Kullanicilar kullanicilar)
        {
            using (ISKUREntities db = new ISKUREntities())
            {
                var userDetails = db.Kullanicilar.Where(x => x.KullaniciAdi == kullanicilar.KullaniciAdi && x.Şifre == kullanicilar.Şifre).FirstOrDefault();
                if (userDetails == null)
                {
                    kullanicilar.GirisEkraniHataMesaji = "Kullanici adi veya şifresi yanlış";

                    return View("GirisS", kullanicilar);
                }
                else
                {
                    Session["KullaniciID"] = userDetails.KullaniciID;
                    Session["KullaniciAdi"] = userDetails.KullaniciAdi;
                    return RedirectToAction("Anasayfa", "Sayfalar");
                }
            }
        }
        public ActionResult CikisYap()
        {
            int kullaniciId = (int)Session["KullaniciID"];
            Session.Abandon();
            return RedirectToAction("Anasayfa", "Sayfalar");
        }

      
    }
}

   