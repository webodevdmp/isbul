﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebTasarimOdevi.Models;

namespace WebTasarimOdevi.Controllers
{
    public class IlanlarTblsController : Controller
    {
        private ISKUREntities db = new ISKUREntities();

        // GET: IlanlarTbls
        public ActionResult Index()
        {
            return View(db.IlanlarTbl.ToList());
        }

        // GET: IlanlarTbls/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IlanlarTbl ilanlarTbl = db.IlanlarTbl.Find(id);
            if (ilanlarTbl == null)
            {
                return HttpNotFound();
            }
            return View(ilanlarTbl);
        }

        // GET: IlanlarTbls/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IlanlarTbls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ad,SoyAd,KullaniciID,ArananMeslek,EgitimDurumu,Maas,Sehir,SirketAdi,IsTanimi,MailAdresi")] IlanlarTbl ilanlarTbl)
        {
            if (ModelState.IsValid)
            {
                db.IlanlarTbl.Add(ilanlarTbl);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ilanlarTbl);
        }

        // GET: IlanlarTbls/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IlanlarTbl ilanlarTbl = db.IlanlarTbl.Find(id);
            if (ilanlarTbl == null)
            {
                return HttpNotFound();
            }
            return View(ilanlarTbl);
        }

        // POST: IlanlarTbls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ad,SoyAd,KullaniciID,ArananMeslek,EgitimDurumu,Maas,Sehir,SirketAdi,IsTanimi,MailAdresi")] IlanlarTbl ilanlarTbl)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ilanlarTbl).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ilanlarTbl);
        }

        // GET: IlanlarTbls/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IlanlarTbl ilanlarTbl = db.IlanlarTbl.Find(id);
            if (ilanlarTbl == null)
            {
                return HttpNotFound();
            }
            return View(ilanlarTbl);
        }

        // POST: IlanlarTbls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            IlanlarTbl ilanlarTbl = db.IlanlarTbl.Find(id);
            db.IlanlarTbl.Remove(ilanlarTbl);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
