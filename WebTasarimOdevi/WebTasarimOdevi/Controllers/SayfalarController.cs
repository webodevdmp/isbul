﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebTasarimOdevi.Controllers
{
    public class SayfalarController : Controller
    {
        // GET: Sayfalar
        public ActionResult Anasayfa()
        {
            return View();
        }
        public ActionResult Hakkimizda()
        {
            return View();
        }
        public ActionResult Organizasyonlar()
        {
            return View();
        }
        public ActionResult Istatistikler()
        {
            return View();
        }
        public ActionResult GVOneriler()
        {
            return View();
        }
        public ActionResult AIGProgrami()
        {
            return View();
        }
        public ActionResult IssizlikS()
        {
            return View();
        }
        public ActionResult IVMDanismanligi()
        {
            return View();
        }
        public ActionResult Iletisim()
        {
            return View();
        }
        public ActionResult IsbasiE()
        {
            return View();
        }
        public ActionResult EngelliI()
        {
            return View();
        }
        public ActionResult YurtdisiI()
        {
            return View();
        }
        
    }
    
}